Resources powering the `lysergic.media` SyncPlay instance.

#### Proxy:
The proxy removes the superfluous STARTTLS message in order for stunnel to terminate the TLS connection.

SyncPlay Client -> syncplay-proxy (lysergic.media:8999) -> stunnel (TLS [::1]:8997) -> syncplay (PLAIN [::1]:8998)

#### Bridge:
The bridge relays chat messages between SyncPlay and IRC.
